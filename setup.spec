%global setup_version %{version}

Summary: A set of system configuration and setup files
Name: setup
Version: 2.14.5
Release: 4%{?dist}
License: Public Domain
URL: https://pagure.io/setup/
Source0: https://pagure.io/setup/archive/%{name}-%{version}/%{name}-%{name}-%{version}.tar.gz
Patch0001: 0001-remove-csh-from-makefile-test.patch
BuildArch: noarch

BuildRequires: make bash perl-interpreter systemd-rpm-macros
#require system release for saner dependency order
Requires: system-release


%description
The setup package contains a set of important system configuration and
setup files, such as passwd, group, and profile.


%prep
%autosetup -n %{name}-%{name}-%{setup_version} -p1
./generate-sysusers-fragments.sh
./shadowconvert.sh

%build

%check
make check

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/etc/profile.d
cp -ar * %{buildroot}/etc
mkdir -p %(dirname %{buildroot}%{_sysusersdir})
mv %{buildroot}/etc/sysusers.d %{buildroot}%{_sysusersdir}
mv %{buildroot}/etc/lang.sh %{buildroot}/etc/profile.d/

touch %{buildroot}/etc/environment
chmod 0644 %{buildroot}/etc/environment
chmod 0400 %{buildroot}/etc/{shadow,gshadow}
touch %{buildroot}/etc/fstab
mkdir -p %{buildroot}/etc/profile.d
echo "#Add any required envvar overrides to this file, it is sourced from /etc/profile" >%{buildroot}/etc/profile.d/sh.local
# echo "#Add any required envvar overrides to this file, is sourced from /etc/csh.login" >%{buildroot}/etc/profile.d/csh.local
mkdir -p %{buildroot}/etc/motd.d
mkdir -p %{buildroot}/run/motd.d
mkdir -p %{buildroot}/usr/lib/motd.d
touch %{buildroot}/usr/lib/motd
#tmpfiles needed for files in /run
mkdir -p %{buildroot}%{_tmpfilesdir}
echo "f /run/motd 0644 root root -" >%{buildroot}%{_tmpfilesdir}/%{name}.conf
echo "d /run/motd.d 0755 root root -" >>%{buildroot}%{_tmpfilesdir}/%{name}.conf
chmod 0644 %{buildroot}%{_tmpfilesdir}/%{name}.conf

# remove unpackaged files from the buildroot
rm -f %{buildroot}/etc/Makefile
rm -f %{buildroot}/etc/serviceslint
rm -f %{buildroot}/etc/uidgidlint
rm -f %{buildroot}/etc/generate-sysusers-fragments.sh
rm -f %{buildroot}/etc/shadowconvert.sh
rm -f %{buildroot}/etc/setup.spec
rm -f %{buildroot}/etc/uidgid
rm -f %{buildroot}/etc/COPYING
rm -rf %{buildroot}/etc/contrib
# don't need csh config anymore.
rm -f %{buildroot}/etc/csh.cshrc
rm -f %{buildroot}/etc/csh.login
rm -f %{buildroot}/etc/lang.csh

	
# make setup a protected package 
# solution from centos8s setup pkg
# fix https://bugzilla.redhat.com/show_bug.cgi?id=1755997
install -p -d -m 755 %{buildroot}/etc/dnf/protected.d/
touch %{name}.conf
echo setup > %{name}.conf
install -p -c -m 0644 %{name}.conf %{buildroot}/etc/dnf/protected.d/
rm -f %{name}.conf

# throw away useless and dangerous update stuff until rpm will be able to
# handle it. To avoid dependency loop on setup, filesystem and bash
# have to use lua.
# besides, as it's uncommon that newaliases installed before
# setup and bash, acceptable in using os.execute.
%post -p <lua>
for i, name in ipairs({"passwd", "shadow", "group", "gshadow"}) do
    os.remove("/etc/"..name..".rpmnew")
end
if posix.access("/usr/bin/newaliases", "x") then
  local pid = posix.fork()
  if pid == 0 then
    posix.redirect2null(1)
    posix.exec("/usr/bin/newaliases")
  elseif pid > 0 then
    posix.wait(pid)
  end
end

%files
%license COPYING
%doc uidgid
%verify(not md5 size mtime) %config(noreplace) /etc/passwd
%verify(not md5 size mtime) %config(noreplace) /etc/group
%verify(not md5 size mtime) %attr(0000,root,root) %config(noreplace,missingok) /etc/shadow
%verify(not md5 size mtime) %attr(0000,root,root) %config(noreplace,missingok) /etc/gshadow
%verify(not md5 size mtime) %config(noreplace) /etc/subuid
%verify(not md5 size mtime) %config(noreplace) /etc/subgid
%config(noreplace) /etc/services
%verify(not md5 size mtime) %config(noreplace) /etc/exports
%config(noreplace) /etc/aliases
%config(noreplace) /etc/environment
%config(noreplace) /etc/filesystems
%config(noreplace) /etc/host.conf
%verify(not md5 size mtime) %config(noreplace) /etc/hosts
%verify(not md5 size mtime) %config(noreplace) /etc/motd
%dir /etc/motd.d
%ghost %verify(not md5 size mtime) %attr(0644,root,root) /run/motd
%dir /run/motd.d
%verify(not md5 size mtime) %config(noreplace) /usr/lib/motd
%dir /usr/lib/motd.d
%config(noreplace) /etc/printcap
%verify(not md5 size mtime) %config(noreplace) /etc/inputrc
%config(noreplace) /etc/bashrc
%config(noreplace) /etc/profile
%config(noreplace) /etc/protocols
%config(noreplace) /etc/ethertypes
%config(noreplace) /etc/networks
%dir /etc/profile.d
%config(noreplace) /etc/profile.d/sh.local
/etc/profile.d/lang.sh
%config(noreplace) %verify(not md5 size mtime) /etc/shells
%ghost %verify(not md5 size mtime) %config(noreplace,missingok) /etc/fstab	
%{_tmpfilesdir}/%{name}.conf
%{_sysusersdir}/20-setup-groups.conf
%{_sysusersdir}/20-setup-users.conf
/etc/dnf/protected.d/%{name}.conf

# since we don't need csh, comment them.
# waiting for a stable version to removed them all.
# %config(noreplace) /etc/profile.d/csh.local
# /etc/profile.d/lang.{sh,csh}
# %config(noreplace) /etc/csh.login
# %config(noreplace) /etc/csh.cshrc


%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.14.5-4
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.14.5-3
- Rebuilt for loongarch release

* Thu Jan 11 2024 Yi Lin <nilusyi@tencent.com> - 2.14.5-2
- seperate basesystem from setup

* Thu Jan 11 2024 Yi Lin <nilusyi@tencent.com> - 2.14.5-1
- Bump version to 2.14.5

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.14.4-3
- Rebuilt for OpenCloudOS Stream 23.09

* Wed Aug 16 2023 Miaojun Dong <zoedong@tencent.com> - 2.14.4-2
- sync with upstream spec file

* Wed Aug 16 2023 Miaojun Dong <zoedong@tencent.com> - 2.14.4-1
- Bump version to 2.14.4

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.13.9.1-4
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 2.13.9.1-3
- Rebuilt for OpenCloudOS Stream 23

* Thu Mar 09 2023 Xin Cheng <denisecheng@tencent.com> - 2.13.9.1-2
- Update release of subpackage basesystem.

* Tue Mar 07 2023 Yi Lin <nilusyi@tencent.com> - 2.13.9.1-2
- fix empty dist_name.

* Thu Apr 28 2022 Lin Yi <nilusyi@tencent.com> - 2.13.9.1-1
- Package init
